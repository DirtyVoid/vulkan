#include <stdlib.h>
#include <stdint.h>
//#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <SDL2/SDL_image.h>
#include <vulkan/vulkan.h>
#include <cglm/cglm.h>


int main(void) {
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window *window =  SDL_CreateWindow( "VULKAN_TEST", \
	    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN
	);
	
	VkApplicationInfo appInfo = { .sType =  VK_STRUCTURE_TYPE_APPLICATION_INFO,
	.pApplicationName = "Hello Triangle",
	.applicationVersion = VK_MAKE_API_VERSION(1, 0, 0, 0),
	.pEngineName = "SS ENGINE",
	.engineVersion = VK_MAKE_API_VERSION(1, 0, 0, 0),
	.apiVersion = VK_MAKE_API_VERSION(0,1,3,231)
	};
	
	uint32_t sdlExtensionCount;
	SDL_Vulkan_GetInstanceExtensions(window,&sdlExtensionCount,NULL);
	const char** sdlExtensions = (const char**)malloc(sdlExtensionCount*sizeof(char*));
	SDL_Vulkan_GetInstanceExtensions(window,NULL,sdlExtensions);
	
	VkInstanceCreateInfo instanceInfo = { .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	.pApplicationInfo = &appInfo,
	.enabledExtensionCount = sdlExtensionCount,
	.ppEnabledExtensionNames = sdlExtensions,
	.enabledLayerCount = 0	
	};
	VkInstance instance = VK_NULL_HANDLE;;
	// if (vkCreateInstance(&instanceInfo, NULL, &instance) != VK_SUCCESS) {
		// printf("failed to create instance!\n");
	 // }

	SDL_Event e;
	bool quit = false;
	while(!quit) {
		while (SDL_PollEvent(&e)) {
			if( e.type == SDL_QUIT ) quit = true;
		}
	}
	

	vkDestroyInstance(instance, NULL);
	free(sdlExtensions);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
} 
